# xenial-java8

Based on cenote/xenial-base

This Docker image is used to build java projects (ant, maven)

Installed software:

* openjdk-8-jdk-headless
* ant
* maven
